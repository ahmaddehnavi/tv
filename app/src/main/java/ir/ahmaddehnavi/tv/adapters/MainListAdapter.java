package ir.ahmaddehnavi.tv.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import ir.ahmaddehnavi.tv.R;
import ir.ahmaddehnavi.tv.activities.PlayerActivity;
import ir.ahmaddehnavi.tv.webservice.models.Tv;

/**
 * Created by ahmad on 7/2/2017.
 */

public class MainListAdapter extends RecyclerView.Adapter<MainListAdapter.ListTvViewHolder> {
    private final LayoutInflater inflater;
    private Context context;
    private final Tv[] tvs;

    public MainListAdapter(Context context, Tv[] tvs) {
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.tvs = tvs;
    }

    @Override
    public ListTvViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ListTvViewHolder(inflater.inflate(R.layout.item_list_tv, parent, false));
    }

    @Override
    public void onBindViewHolder(ListTvViewHolder holder, int position) {
        holder.bind(tvs[position]);
    }

    @Override
    public int getItemCount() {
        return tvs.length;
    }


    class ListTvViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private final ImageView imageViewIcon;
        private final TextView textViewTitle;
        private final TextView textViewCurrentProgram;
        private Tv tv;

        public ListTvViewHolder(View itemView) {
            super(itemView);
            imageViewIcon = (ImageView) itemView.findViewById(R.id.imageViewIcon);
            textViewTitle = (TextView) itemView.findViewById(R.id.textViewTitle);
            textViewCurrentProgram = (TextView) itemView.findViewById(R.id.textViewCurrentProgram);
            itemView.setOnClickListener(this);
        }

        public void bind(Tv tv) {
            this.tv = tv;
            Glide.with(imageViewIcon.getContext())
                    .load(tv.getImg()).into(imageViewIcon);
            textViewTitle.setText(tv.getTitle());
            textViewCurrentProgram.setText(tv.getPrograms().get(0).getTitle());
        }

        @Override
        public void onClick(View v) {
            PlayerActivity.start(context, getAdapterPosition(), 0);
        }
    }

}
