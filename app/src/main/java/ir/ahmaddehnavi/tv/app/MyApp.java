package ir.ahmaddehnavi.tv.app;

import android.app.Application;

import java.util.Locale;

import ir.ahmaddehnavi.tv.utils.LocaleUtil;

/**
 * Created by ahmad on 7/2/2017.
 */

public class MyApp extends Application {

//    protected String userAgent;

    @Override
    public void onCreate() {
        super.onCreate();
        LocaleUtil.setLocale(this, new Locale("fa"));
    }

}
