package ir.ahmaddehnavi.tv.fragments;

import android.arch.lifecycle.LifecycleFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import ir.ahmaddehnavi.tv.R;
import ir.ahmaddehnavi.tv.webservice.Repository;
import ir.ahmaddehnavi.tv.webservice.repository.RepositoryBase;

public class AboutFragment extends LifecycleFragment {

    public static final String TAG = AboutFragment.class.getName();
    protected TextView textViewAboutUs;
    protected ProgressBar progressBarLoading;

    public AboutFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_about, container, false);
        initView(rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Repository.getInstance(getContext()).getAboutUs(this, new RepositoryBase.ApiListener<String>() {
            @Override
            public void onLoadingChanged(boolean isLoading, boolean isDataNotifiedBefore) {
                progressBarLoading.setVisibility((isLoading && !isDataNotifiedBefore) ? View.VISIBLE : View.GONE);
            }

            @Override
            public void onDataChanged(String data, RepositoryBase.SourceType sourceType) {
                textViewAboutUs.setText(data);
            }

            @Override
            public void onError(Throwable e, boolean isDataNotifiedBefore) {
                textViewAboutUs.setText(R.string.default_about_us);
                Toast.makeText(getContext().getApplicationContext(), R.string.error_load_about_us, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void initView(View rootView) {
        textViewAboutUs = (TextView) rootView.findViewById(R.id.TextViewAboutUs);
        progressBarLoading = (ProgressBar) rootView.findViewById(R.id.progressBarLoading);
    }
}
