package ir.ahmaddehnavi.tv.fragments;


import android.arch.lifecycle.LifecycleFragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import ir.ahmaddehnavi.tv.R;
import ir.ahmaddehnavi.tv.adapters.MainListAdapter;
import ir.ahmaddehnavi.tv.webservice.Repository;
import ir.ahmaddehnavi.tv.webservice.models.Tv;
import ir.ahmaddehnavi.tv.webservice.repository.RepositoryBase;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListProgramFragment extends LifecycleFragment {


    public static final String TAG = ListProgramFragment.class.getName();
    protected View rootView;
    protected RecyclerView recyclerView;
    protected ProgressBar progressBarLoading;

    public ListProgramFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_main, container, false);
        initView(rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadTvList();
    }

    private void loadTvList() {
        Repository.getInstance(getContext()).getProgramList(this, new RepositoryBase.ApiListener<Tv[]>() {
            @Override
            public void onLoadingChanged(boolean isLoading, boolean isDataNotifiedBefore) {
                progressBarLoading.setVisibility(isLoading && !isDataNotifiedBefore ? View.VISIBLE : View.GONE);
            }

            @Override
            public void onDataChanged(Tv[] tvs, RepositoryBase.SourceType sourceType) {
                recyclerView.setAdapter(new MainListAdapter(getContext(), tvs));
            }

            @Override
            public void onError(Throwable e, boolean isDataNotifiedBefore) {

            }
        });
    }

    private void initView(View rootView) {
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        progressBarLoading = (ProgressBar) rootView.findViewById(R.id.progressBarLoading);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }

}
