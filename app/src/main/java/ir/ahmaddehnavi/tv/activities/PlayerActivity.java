package ir.ahmaddehnavi.tv.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayer;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.source.hls.HlsMediaSource;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.google.android.exoplayer2.upstream.HttpDataSource;
import com.google.android.exoplayer2.util.Util;

import java.util.Date;
import java.util.List;

import ir.ahmaddehnavi.tv.R;
import ir.ahmaddehnavi.tv.utils.TimeUtil;
import ir.ahmaddehnavi.tv.webservice.Repository;
import ir.ahmaddehnavi.tv.webservice.models.Program;
import ir.ahmaddehnavi.tv.webservice.models.Tv;
import ir.ahmaddehnavi.tv.webservice.repository.RepositoryBase;

public class PlayerActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = PlayerActivity.class.getName();
    private static final String EXTRA_TV_INDEX = "extra_tv_index";
    private static final String EXTRA_PROGRAM_INDEX = "extra_p_index";

    private static final DefaultBandwidthMeter BANDWIDTH_METER = new DefaultBandwidthMeter();

    private SimpleExoPlayerView playerView;
    private TextView textViewNext;
    private TextView textViewCurrent;
    private TextView textViewPre;
    private TextView textViewTime;
    private ComponentListener componentListener;
    private DataSource.Factory mediaDataSourceFactory;
    private Handler mainHandler;
    private DefaultTrackSelector trackSelector;
    private String userAgent;
    private SimpleExoPlayer player;

    private int tvIndex;
    private int currentProgramIndex;
    private Tv currentTV;

    private String buildVideoUrl(String baseVideoUrl, String time) {
        if (baseVideoUrl == null || time == null) return null;
        return baseVideoUrl + "?start=" + time;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        tvIndex = getIntent().getIntExtra(EXTRA_TV_INDEX, 0);
        currentProgramIndex = getIntent().getIntExtra(EXTRA_PROGRAM_INDEX, 0);


        initView();
        initPlayerRequirement();
        Repository.getInstance(this).getProgramList(this, new RepositoryBase.ApiListener<Tv[]>() {
            @Override
            public void onLoadingChanged(boolean isLoading, boolean isDataNotifiedBefore) {

            }

            @Override
            public void onDataChanged(Tv[] tvs, RepositoryBase.SourceType sourceType) {
                PlayerActivity.this.currentTV = tvs[tvIndex];

                initializePlayer();
                initController();
            }

            @Override
            public void onError(Throwable e, boolean isDataNotifiedBefore) {

            }
        });
    }

    private void initController() {
        List<Program> programs = currentTV.getPrograms();
        Program currentProgram = programs.get(currentProgramIndex);
        textViewCurrent.setText(currentProgram.getTitle());
        if (currentProgramIndex - 1 >= 0) {
            Program preProgram = programs.get(currentProgramIndex - 1);
            textViewPre.setText(preProgram.getTitle());
            textViewPre.setVisibility(View.VISIBLE);
        } else
            textViewPre.setVisibility(View.INVISIBLE);

        if (currentProgramIndex + 1 < programs.size()) {
            Program nextProgram = programs.get(currentProgramIndex + 1);
            textViewNext.setText(nextProgram.getTitle());
            textViewNext.setVisibility(View.VISIBLE);
        } else
            textViewNext.setVisibility(View.INVISIBLE);

        textViewTime.setText(TimeUtil.formatDuration(currentProgram.getEnd() - currentProgram.getStart()));

//        SeekBar seekBar = (SeekBar) findViewById(R.id.seekBar);
//        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
//            @Override
//            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
//                if (fromUser) {
//                    long duration = currentProgram.getEnd() - currentProgram.getStart();
//                    initializePlayer(progress * duration);
//                }
//            }
//
//            @Override
//            public void onStartTrackingTouch(SeekBar seekBar) {
//
//            }
//
//            @Override
//            public void onStopTrackingTouch(SeekBar seekBar) {
//
//            }
//        });

//        player.addListener(new ComponentListener() {
//            @Override
//            public void onTimelineChanged(Timeline timeline, Object manifest) {
//                super.onTimelineChanged(timeline, manifest);
//                if (manifest instanceof HlsManifest) {
//                    HlsManifest hlsManifest = (HlsManifest) manifest;
//                    int percent = (int) (player.getCurrentPosition() * 100 / player.getDuration());
////                    seekBar.setProgress(percent);todo fixme percent is not correct
////                    String duration = TimeUtil.formatDuration();
////                    long startOffsetUs = hlsManifest.mediaPlaylist.startOffsetUs;
////                timeBar.setPosition();
//                }
//
//            }
//        });
    }

    private void initPlayerRequirement() {
        userAgent = Util.getUserAgent(this, getString(R.string.app_name));
        mediaDataSourceFactory = buildHttpDataSourceFactory(true);
        mainHandler = new Handler();
        componentListener = new ComponentListener();
        trackSelector = new DefaultTrackSelector(BANDWIDTH_METER);
    }

    private DataSource.Factory buildDataSourceFactory(boolean useBandwidthMeter) {
        DefaultBandwidthMeter bandwidthMeter = useBandwidthMeter ? BANDWIDTH_METER : null;
        return new DefaultDataSourceFactory(this, bandwidthMeter,
                new DefaultHttpDataSourceFactory(userAgent, bandwidthMeter));
    }

    private void initializePlayer() {
        initializePlayer(0);
    }

    /**
     * @param seekTime second
     */
    private void initializePlayer(long seekTime) {
        long startTimeStamp = currentTV.getPrograms().get(currentProgramIndex).getStart() + seekTime;

        Date date = new Date(startTimeStamp * 1000);
        String s = date.toString();
        Log.d(TAG, "initializePlayer: try play video at [" + s + "]");

        releasePlayer();
        String videoUri = buildVideoUrl(currentTV.getUrl(),
                String.valueOf(startTimeStamp));
        player = ExoPlayerFactory.newSimpleInstance(this, trackSelector);
        player.addListener(componentListener);
        MediaSource mediaSource = buildMediaSource(Uri.parse(videoUri));
        player.setPlayWhenReady(true);
        player.prepare(mediaSource, true, false);
        playerView.setPlayer(player);
    }

    private MediaSource buildMediaSource(Uri uri) {
        return new HlsMediaSource(uri, mediaDataSourceFactory, mainHandler, null);
    }

    private void releasePlayer() {
        if (player != null) {
            player.removeListener(componentListener);
            player.release();
            player = null;
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        if (Util.SDK_INT > 23) {
            initializePlayer();
        }
    }


    @Override
    public void onResume() {
        super.onResume();

    }


    @Override
    public void onPause() {
        super.onPause();
        if (Util.SDK_INT <= 23) {
            releasePlayer();
        }
    }


    @Override
    public void onStop() {
        super.onStop();
        if (Util.SDK_INT > 23) {
            releasePlayer();
        }
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.textViewNext) {
            if (currentProgramIndex + 1 < currentTV.getPrograms().size())
                currentProgramIndex++;
            initController();
            initializePlayer();
        } else if (view.getId() == R.id.textViewPre) {
            if (currentProgramIndex - 1 >= 0)
                currentProgramIndex--;
            initController();
            initializePlayer();
        }
    }


    private class ComponentListener implements ExoPlayer.EventListener {

        @Override
        public void onPlayerStateChanged(boolean playWhenReady,
                                         int playbackState) {
            String stateString;
            switch (playbackState) {
                case ExoPlayer.STATE_IDLE:
                    stateString = "ExoPlayer.STATE_IDLE      -";
                    break;
                case ExoPlayer.STATE_BUFFERING:
                    stateString = "ExoPlayer.STATE_BUFFERING -";
                    break;
                case ExoPlayer.STATE_READY:
                    stateString = "ExoPlayer.STATE_READY     -";
                    break;
                case ExoPlayer.STATE_ENDED:
                    stateString = "ExoPlayer.STATE_ENDED     -";
                    break;
                default:
                    stateString = "UNKNOWN_STATE             -";
                    break;
            }
            Log.d("ExopLayer", "changed state to " + stateString
                    + " playWhenReady: " + playWhenReady);
        }

        @Override
        public void onTimelineChanged(Timeline timeline, Object manifest) {
        }

        @Override
        public void onTracksChanged(TrackGroupArray trackGroups,
                                    TrackSelectionArray trackSelections) {
        }

        @Override
        public void onLoadingChanged(boolean isLoading) {
        }

        @Override
        public void onPlayerError(ExoPlaybackException error) {
        }

        @Override
        public void onPositionDiscontinuity() {
        }

        @Override
        public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {
        }
    }

    private HttpDataSource.Factory buildHttpDataSourceFactory(boolean useBandwidthMeter) {
        return new DefaultHttpDataSourceFactory(userAgent, useBandwidthMeter ? BANDWIDTH_METER : null);
    }

    public static void start(Context context, int tvIndex, int programIndex) {
        Intent starter = new Intent(context, PlayerActivity.class);
        starter.putExtra(EXTRA_PROGRAM_INDEX, programIndex);
        starter.putExtra(EXTRA_TV_INDEX, tvIndex);
        context.startActivity(starter);
    }

    private void initView() {
        playerView = (SimpleExoPlayerView) findViewById(R.id.playerView);
        textViewNext = (TextView) findViewById(R.id.textViewNext);
        textViewNext.setOnClickListener(PlayerActivity.this);
        textViewCurrent = (TextView) findViewById(R.id.textViewCurrent);
        textViewPre = (TextView) findViewById(R.id.textViewPre);
        textViewPre.setOnClickListener(PlayerActivity.this);
        textViewTime = (TextView) findViewById(R.id.textViewTime);

    }
}
