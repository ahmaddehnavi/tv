package ir.ahmaddehnavi.tv.activities;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import ir.ahmaddehnavi.tv.R;
import ir.ahmaddehnavi.tv.fragments.AboutFragment;
import ir.ahmaddehnavi.tv.fragments.ListProgramFragment;

public class MainActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    protected Toolbar toolbar;
    protected NavigationView navView;
    protected DrawerLayout drawerLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        super.setContentView(R.layout.activity_main);
        initView();
        initToolbar();
        initDrawer();
        showMainFragment();
    }

    private void initDrawer() {
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();
        navView.setNavigationItemSelectedListener(this);
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
    }



    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_main) {
            showMainFragment();
        } else if (id == R.id.nav_about) {
            showAboutFragment();
        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    private void showAboutFragment() {
        getSupportFragmentManager()
                .beginTransaction().replace(R.id.container, new AboutFragment(),AboutFragment.TAG)
//                .addToBackStack(AboutFragment.TAG)
                .commit();
    }

    private void showMainFragment() {
        getSupportFragmentManager()
                .beginTransaction().replace(R.id.container, new ListProgramFragment())
//                .addToBackStack(ListProgramFragment.TAG)
                .commit();
    }

    private void initView() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        navView = (NavigationView) findViewById(R.id.nav_view);
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
//            if (getSupportFragmentManager().getBackStackEntryCount() > 1)
//                getSupportFragmentManager().popBackStack();
//            else
                super.onBackPressed();
        }
    }
}
