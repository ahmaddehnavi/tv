package ir.ahmaddehnavi.tv.utils;

import android.annotation.SuppressLint;

/**
 * Created by ahmad on 7/2/2017.
 */

public class TimeUtil {
    @SuppressLint("DefaultLocale")
    public static String formatDuration(long s) {
        return String.format("%d:%02d:%02d", s / 3600, (s % 3600) / 60, (s % 60));
    }
}
