
package ir.ahmaddehnavi.tv.webservice.models;

import com.google.gson.annotations.SerializedName;

public class Program{

    @SerializedName("title")
    private String title;

    @SerializedName("start")
    private long start;

    @SerializedName("end")
    private long end;

    private Tv tv;

    public Tv getTv() {
        return tv;
    }

    public void setTv(Tv tv) {
        this.tv = tv;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public long getStart() {
        return start;
    }

    public void setStart(long start) {
        this.start = start;
    }

    /**
     * @return end time in unix timestamp
     */
    public long getEnd() {
        return end;
    }

    public void setEnd(long end) {
        this.end = end;
    }

}
