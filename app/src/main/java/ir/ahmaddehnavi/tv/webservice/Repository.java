package ir.ahmaddehnavi.tv.webservice;

import android.arch.lifecycle.LifecycleOwner;
import android.content.Context;
import android.text.format.DateUtils;

import ir.ahmaddehnavi.tv.webservice.httpclient.RestClient;
import ir.ahmaddehnavi.tv.webservice.httpclient.RetrofitRestClient;
import ir.ahmaddehnavi.tv.webservice.models.GetAboutResponse;
import ir.ahmaddehnavi.tv.webservice.models.ListProgramsResponse;
import ir.ahmaddehnavi.tv.webservice.models.Tv;
import ir.ahmaddehnavi.tv.webservice.repository.RepositoryBase;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by ahmad on 7/2/2017.
 */

public class Repository extends RepositoryBase {

    private static Repository instance;
    private final RetrofitRestClient client;
    private final CacheManagement memoryCache;

    public static Repository getInstance(Context context) {
        if (instance == null) instance = new Repository(context.getApplicationContext());
        return instance;
    }

    private Repository(Context context) {
        client = RestClient.getClient(context);
        memoryCache = new CacheManagement();
    }

    private Tv[] responseToTvs(ListProgramsResponse response) {
        return response.values().toArray(new Tv[0]);
    }

    public void getProgramList(LifecycleOwner owner, ApiListener<Tv[]> listener) {
        String key = "getProgramList";
        RepositoryNotifier<Tv[]> notifier = new RepositoryNotifier<>(owner, listener);

        RepositoryBase.Cache cache = memoryCache.get(key);
        if (cache != null) {
            Object data = cache.getData();
            if (data instanceof Tv[]) {
                Tv[] cachedTvs = (Tv[]) data;
                notifier.notifyDataFromCache(cachedTvs);
                if (!cache.isExpired(DateUtils.MINUTE_IN_MILLIS * 5)) return;
            }
        }

        client.getProgramList().enqueue(new Callback<ListProgramsResponse>() {
            @Override
            public void onResponse(Call<ListProgramsResponse> call, Response<ListProgramsResponse> response) {
                Tv[] tvs = responseToTvs(response.body());
                memoryCache.put(new Cache(key, tvs));
                notifier.notifyDataFromWeb(tvs);
            }

            @Override
            public void onFailure(Call<ListProgramsResponse> call, Throwable t) {
                notifier.notifyError(t);
            }
        });
    }

    public void getAboutUs(LifecycleOwner owner, ApiListener<String> listener) {

        String key="getAboutUs";
        RepositoryNotifier<String> notifier = new RepositoryNotifier<>(owner, listener);

        RepositoryBase.Cache cache = memoryCache.get(key);
        if (cache != null) {
            Object data = cache.getData();
            if (data instanceof String) {
                String cachedAboutUs = (String) data;
                notifier.notifyDataFromCache(cachedAboutUs);
                if (!cache.isExpired(DateUtils.MINUTE_IN_MILLIS * 5)) return;
            }
        }
        client.getAboutUs().enqueue(new Callback<GetAboutResponse>() {
            @Override
            public void onResponse(Call<GetAboutResponse> call, retrofit2.Response<GetAboutResponse> response) {
                String about = response.body().getAbout();
                memoryCache.put(new Cache(key, about));
                notifier.notifyDataFromWeb(about);
            }

            @Override
            public void onFailure(Call<GetAboutResponse> call, Throwable t) {
                notifier.notifyError(t);
            }
        });
    }


}
