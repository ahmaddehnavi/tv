package ir.ahmaddehnavi.tv.webservice.httpclient;

import ir.ahmaddehnavi.tv.webservice.models.GetAboutResponse;
import ir.ahmaddehnavi.tv.webservice.models.ListProgramsResponse;
import retrofit2.Call;
import retrofit2.http.GET;

/**
 * Created by Ahmad on 5/29/2017.
 */

public interface RetrofitRestClient {

    @GET("?act=getPrograms")
    Call<ListProgramsResponse> getProgramList();

    @GET("?act=getAbout")
    Call<GetAboutResponse> getAboutUs();
}
