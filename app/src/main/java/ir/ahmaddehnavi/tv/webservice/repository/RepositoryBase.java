package ir.ahmaddehnavi.tv.webservice.repository;

import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.MutableLiveData;

import java.util.HashMap;

/**
 * Created by ahmad on 7/3/2017.
 */

public class RepositoryBase {
//    abstract class Loader<ResultType, RequestType> {
//
//        public abstract LiveData<ResultType> loadFromCache();
//        public abstract void saveToCache(ResultType resultType);
//
//        public abstract LiveData<ResultType> loadFromDB();
//
//        public abstract void saveToDB(ResultType requestType);
//
//        public abstract Call<RequestType> loadFromWeb();
//
//        public abstract boolean shouldLoadFromDB();
//
//        public abstract boolean shouldLoadFromWeb();
//    }

    public static class CacheManagement {

        private HashMap<String, Cache> hashMap = new HashMap<>(50);

        public void put(Cache cache) {
            hashMap.put(cache.key, cache);
            cache.lastModifiedTime = System.currentTimeMillis();
        }

        public Cache get(String key) {
            return hashMap.get(key);
        }
    }

    public class Cache {
        private String key;
        private Object data;
        private long lastModifiedTime;

        public Cache(String key, Object data) {
            this.key = key;
            this.data = data;
        }

        public Object getData() {
            return data;
        }

        public boolean isExpired(long duraion) {
            return System.currentTimeMillis() - lastModifiedTime > duraion;
        }

        public Cache ifExpired(long duraion, Runnable runnable) {
            if (isExpired(duraion))
                runnable.run();
            return this;
        }

    }

    protected class RepositoryNotifier<T> {
        private final MutableLiveData<ApiResult<T>> liveData;
        private boolean isDataNotified = false;

        public RepositoryNotifier(LifecycleOwner owner, ApiListener<T> listener) {
            liveData = new MutableLiveData<>();
            liveData.observe(owner, apiResult -> {
                if (apiResult != null)
                    apiResult.notify(listener);
            });
        }

        public void notifyLoading() {
            liveData.postValue(new LoadingApiResult<>(isDataNotified));
        }

        public void notifyError(Throwable error) {
            liveData.postValue(new ErrorApiResult<>(error, isDataNotified));
        }

        public void notifyDataFromWeb(T data) {
            isDataNotified = data != null;
            liveData.postValue(new DataApiResult<>(data, SourceType.WEB));
        }

        public void notifyDataFromDB(T data) {
            isDataNotified = data != null;
            liveData.postValue(new DataApiResult<>(data, SourceType.DB));
        }

        public void notifyDataFromCache(T data) {
            isDataNotified = data != null;
            liveData.postValue(new DataApiResult<>(data, SourceType.RAM));
        }

    }

    protected interface ApiResult<T> {
        void notify(ApiListener<T> listener);
    }

    protected class LoadingApiResult<T> implements ApiResult<T> {
        private boolean isDataNotified;

        public LoadingApiResult(boolean isDataNotified) {
            this.isDataNotified = isDataNotified;
        }

        @Override
        public void notify(ApiListener listener) {
            listener.onLoadingChanged(true, isDataNotified);
        }
    }

    protected class DataApiResult<T> implements ApiResult<T> {
        private T data;
        private SourceType sourceType;

        public DataApiResult(T data, SourceType sourceType) {
            this.data = data;
            this.sourceType = sourceType;
        }

        @Override
        public void notify(ApiListener<T> listener) {
            listener.onLoadingChanged(false, true);
            listener.onDataChanged(data, sourceType);
        }
    }


    protected class ErrorApiResult<T> implements ApiResult<T> {
        private Throwable error;
        private boolean isDataNotified;

        private ErrorApiResult(Throwable error, boolean isDataNotified) {
            this.error = error;
            this.isDataNotified = isDataNotified;
        }

        @Override
        public void notify(ApiListener listener) {
            listener.onLoadingChanged(false, isDataNotified);
            listener.onError(error, isDataNotified);
        }
    }

    public interface ApiListener<T> {
        void onLoadingChanged(boolean isLoading, boolean isDataNotifiedBefore);

        void onDataChanged(T data, SourceType sourceType);

        void onError(Throwable e, boolean isDataNotifiedBefore);
    }

    public class ApiListenerImpl<T> implements ApiListener<T> {
        @Override
        public void onLoadingChanged(boolean isLoading, boolean isDataNotifiedBefore) {
        }

        @Override
        public void onDataChanged(T data, SourceType sourceType) {
        }

        @Override
        public void onError(Throwable e, boolean isDataNotifiedBefore) {
        }
    }

    public enum SourceType {
        RAM, DB, WEB
    }
}
