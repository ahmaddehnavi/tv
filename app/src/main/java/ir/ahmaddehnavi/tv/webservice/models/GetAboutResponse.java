package ir.ahmaddehnavi.tv.webservice.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by ahmad on 7/3/2017.
 */

public class GetAboutResponse {
    @SerializedName("about")
    private String about;

    public String getAbout() {
        return about;
    }

    public void setAbout(String about) {
        this.about = about;
    }
}
