package ir.ahmaddehnavi.tv.webservice.httpclient;

import android.content.Context;

import ir.ahmaddehnavi.tv.app.Config;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * Created by Ahmad on 5/29/2017.
 */

public class RestClient {
    private static RetrofitRestClient webService;

    private static void init(final Context context) {
        OkHttpClient client = new OkHttpClient.Builder().build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Config.API_BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        webService = retrofit.create(RetrofitRestClient.class);
    }

    public static RetrofitRestClient getClient(Context context) {
        if (webService == null) init(context);
        return webService;
    }
}
